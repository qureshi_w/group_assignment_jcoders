/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab_8.analytics;

/**
 *
 * @author Riteeka
 */
public class ClassComparable implements Comparable<ClassComparable>{

     private int userId;
     private int noOfPosts;
    
    public ClassComparable(int userIdInj, int posts){
        
        this.userId = userIdInj;
        this.noOfPosts = posts;
        
    }
    
    @Override
    public int compareTo(ClassComparable t) {
        
      if(this.noOfPosts == t.noOfPosts){
          return 0;
        }else if(this.noOfPosts > t.noOfPosts){
             return 1;        
        }else{            
            return -1;
        }
      }

   
    public int getUserId() {
        return userId;
    }

    public int getNumberOfPosst() {
        return noOfPosts;
    }
    
    
    
}