/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab_8.analytics;

import java.util.ArrayList;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lab_8.entities.Comment;
import lab_8.entities.Post;
import lab_8.entities.User;

/**
 *
 * @author harshalneelkamal
 */
public class AnalysisHelper {

    public void userWithMostLikes() {

        Map<Integer, Integer> userLikecount = new HashMap<Integer, Integer>();

        Map<Integer, User> users = DataStore.getInstance().getUsers();
        for (User user : users.values()) {
            for (Comment c : user.getComments()) {
                int likes = 0;
                if (userLikecount.containsKey(user.getId())) {
                    likes = userLikecount.get(user.getId());
                }
                likes += c.getLikes();
                userLikecount.put(user.getId(), likes);
            }
        }
        int max = 0;
        int maxId = 0;

        for (int id : userLikecount.keySet()) {
            if (userLikecount.get(id) > max) {
                max = userLikecount.get(id);
                maxId = id;
            }
        }
        System.out.println("User with most likes : " + max + "\n" + users.get(maxId));
    }

    public void getFiveMostLikedComment() {
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();

        List<Comment> commentList = new ArrayList<>(comments.values());

        Collections.sort(commentList, new Comparator<Comment>() {
            @Override
            public int compare(Comment o1, Comment o2) {

                return o2.getLikes() - o1.getLikes();
            }
        });

        System.out.println("5 most liked comments : ");
        for (int i = 0; i < commentList.size() && i < 5; i++) {
            System.out.println(commentList.get(i));
        }
    }

    public void getAverageNumOfLikesPerComment() {

        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        List<Comment> commentList = new ArrayList<>(comments.values());
        float likes = 0;
        for (Comment c : comments.values()) {

            likes += c.getLikes();
        }
        System.out.println("Average" + likes / commentList.size());
    }

    public void getPostWithMostLikedComments() {
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();

        List<Comment> commentList = new ArrayList<>(comments.values());

        Collections.sort(commentList, new Comparator<Comment>() {
            @Override
            public int compare(Comment o1, Comment o2) {
                return o2.getLikes() - o1.getLikes();
            }
        });

        System.out.println("Post with most liked commnets : ");
        for (int i = 0; i < commentList.size() && i < 1; i++) {
            System.out.println(commentList.get(i));
        }
    }

    public void getPostWithMostComments() {

        Map<Integer, Post> post = DataStore.getInstance().getPosts();
//        int maxComments = 0;
//        int postId = 0;
//        for (Post p : post.values()) {
//            if (p.getComments().size() > maxComments) {
//                maxComments = p.getComments().size();
//                postId = p.getPostId();
//            }
//        }
//        System.out.println("Max Post  : " + maxComments + "\n Post ID" + postId);

        List<Post> postList = new ArrayList<>(post.values());

        Collections.sort(postList, new Comparator<Post>() {
            @Override
            public int compare(Post p1, Post p2) {
                return p2.getComments().size() - p1.getComments().size();
            }
        });  
        System.out.println("Post ID " + postList.get(0).getPostId() + "\n Post Comments" + postList.get(0).getComments().size());

    }
    
     public void getFiveMostInactiveUsersBasedOnPosts(){
       
       Map<Integer, Post> posts = DataStore.getInstance().getPosts();
       Map<Integer, Set<Integer>> userPost = new HashMap<>();
       ArrayList<ClassComparable> arrayList = new ArrayList<>();
       
      for(Post post : posts.values()){
          
          if(!userPost.containsKey(post.getUserId())){
              userPost.put(post.getUserId(), new HashSet());
              userPost.get(post.getUserId()).add(post.getPostId());
          }
          else{
              userPost.get(post.getUserId()).add(post.getPostId());
          }
      
      }
          for(int i : userPost.keySet()){
              ClassComparable compare = new ClassComparable(i, userPost.get(i).size());
              arrayList.add(compare);
          }
          
          Collections.sort(arrayList);
          System.out.println("*************Top 5 inactive users based on posts**************");
          for(int i =0; i<5 ; i++)
        {
            System.out.println("User Id:" +arrayList.get(i).getUserId() +"," + "posts:" +arrayList.get(i).getNumberOfPosst());
        }
          
      }
       
     
       public void getFiveMostInactiveUsersBasedOnComments(){
  
       Map<Integer, User> users = DataStore.getInstance().getUsers();
       Map<Integer, Set<Integer>> comments = new HashMap<>();
       ArrayList<ClassComparable> arrayList = new ArrayList<>();
       
       for(int i : users.keySet()){
           ClassComparable comparable = new ClassComparable(i, users.get(i).getComments().size());
           arrayList.add(comparable);
       }
       
       Collections.sort(arrayList);
       System.out.println("*************Top 5 inactive users based on comments**************");
        for(int i =0; i<5 ; i++)
        {
            System.out.println("User Id:" +arrayList.get(i).getUserId() +","+ "comments:" +arrayList.get(i).getNumberOfPosst());
        }
            
   }
       
       public void getTopFiveActiveandProactiveUsersOverall() {
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<Integer, Integer> userCountDetails = new HashMap<Integer, Integer>();
        for (User user : users.values()) {
            int total = 0;
            //int numberOfComments = 0;
            //numberOfComments = user.getComments().size();
            int numberOfLikes = 0;
            int numberOfPosts = 0;
            for (Comment c : user.getComments()) {
                numberOfLikes = numberOfLikes + c.getLikes();  
            }
            for (Post post : posts.values()) {
                if(post.getUserId() == user.getId()){
                numberOfPosts++;
                }
            }            
            //total = numberOfComments + numberOfLikes + numberOfPosts;
            total = numberOfLikes + numberOfPosts;
            userCountDetails.put(user.getId(), total);
        }
        List<Map.Entry<Integer, Integer>> listmostinactive = new LinkedList<Map.Entry<Integer, Integer>>(userCountDetails.entrySet());
        List<Map.Entry<Integer, Integer>> listmostactive = new LinkedList<Map.Entry<Integer, Integer>>(userCountDetails.entrySet());

        
	Collections.sort(listmostinactive, new Comparator<Map.Entry<Integer, Integer>>() {
		public int compare(Map.Entry<Integer, Integer> o1,
				Map.Entry<Integer, Integer> o2) {
			return (o1.getValue()).compareTo(o2.getValue());
		}
	});
        System.out.println("Top 5 inactive users based on Overall activity");
	int printCount = 0;
	for (Map.Entry<Integer, Integer> entry : listmostinactive) {
		if (printCount == 5) {
			break;
		}
		System.out.println("User ID  = " + entry.getKey() + ", Total Like, Post and Comment Count = " + entry.getValue());
		printCount++;
	}
        Collections.sort(listmostactive, new Comparator<Map.Entry<Integer, Integer>>() {
		public int compare(Map.Entry<Integer, Integer> o2,
				Map.Entry<Integer, Integer> o1) {
			return (o1.getValue()).compareTo(o2.getValue());
		}
	});
        System.out.println("Top 5 active users based on Overall activity");
	int printCount1 = 0;
	for (Map.Entry<Integer, Integer> entry : listmostactive) {
		if (printCount1 == 5) {
			break;
		}
		System.out.println("User ID  = " + entry.getKey() + ", Total Likes, Post and Comment Count = " + entry.getValue());
		printCount1++;
	}
    }
  
       
}
       
      
   
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
     
        
   

