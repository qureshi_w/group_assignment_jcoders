/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4.analyticsHelper;

import assignment_4.entities.*;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Waqar
 */
public class DataStore {

    private static DataStore dataStoreRef;
    private Map<Integer, Customer> customersEntityMap;
    private Map<Integer, Item> itemEntityMap;
    private Map<Integer, Order> orderEntityMap;
    private Map<Integer, Product> productEntityMap;
    private Map<Integer, SalesPerson> salesPersonEntityMap;

    // Making constructer Private 
    private DataStore() {
        this.customersEntityMap = new HashMap<>();
        this.itemEntityMap = new HashMap<>();
        this.orderEntityMap = new HashMap<>();
        this.productEntityMap = new HashMap<>();
        this.salesPersonEntityMap = new HashMap<>();
    }

    // Singleton Method to get Instance
    public static DataStore getInstance() {
        if (dataStoreRef == null) {
            dataStoreRef = new DataStore();
        }
        return dataStoreRef;
    }

    public static DataStore getDataStoreRef() {
        return dataStoreRef;
    }

    public static void setDataStoreRef(DataStore dataStoreRef) {
        DataStore.dataStoreRef = dataStoreRef;
    }

    public Map<Integer, Customer> getCustomersEntityMap() {
        return customersEntityMap;
    }

    public void setCustomersEntityMap(Map<Integer, Customer> customersEntityMap) {
        this.customersEntityMap = customersEntityMap;
    }

    public Map<Integer, Item> getItemEntityMap() {
        return itemEntityMap;
    }

    public void setItemEntityMap(Map<Integer, Item> itemEntityMap) {
        this.itemEntityMap = itemEntityMap;
    }

    public Map<Integer, Order> getOrderEntityMap() {
        return orderEntityMap;
    }

    public void setOrderEntityMap(Map<Integer, Order> orderEntityMap) {
        this.orderEntityMap = orderEntityMap;
    }

    public Map<Integer, Product> getProductEntityMap() {
        return productEntityMap;
    }

    public void setProductEntityMap(Map<Integer, Product> productEntityMap) {
        this.productEntityMap = productEntityMap;
    }

    public Map<Integer, SalesPerson> getSalesPersonEntityMap() {
        return salesPersonEntityMap;
    }

    public void setSalesPersonEntityMap(Map<Integer, SalesPerson> salesPersonEntityMap) {
        this.salesPersonEntityMap = salesPersonEntityMap;
    }

  

}
