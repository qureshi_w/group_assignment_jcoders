/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4.analyticsHelper;

import assignment_4.entities.*;
import java.util.*;

/**
 *
 * @author Waqar
 */
public class Analysis {

    public void gettopthreeMostPopularProducts() {

        Map<Integer, Item> itemsMap = DataStore.getInstance().getItemEntityMap();
        Map<Integer, Integer> popularProductsMap = new HashMap<>();
        Map<Integer, Product> prodductMap = DataStore.getInstance().getProductEntityMap();

        for (Item item : itemsMap.values()) {
            int revenue = 0;
            int salesPrice = item.getSalesPrice();
            int quantity = item.getQuantity();
            int minPrice = prodductMap.get(item.getProductId()).getMinPrice();
            // Calculate actual price
            int actualSalesPrice = salesPrice - minPrice;
            revenue = (actualSalesPrice) * quantity;

            // If prodcut is assocaite more than one iten the add revenue
            if (popularProductsMap.containsKey(item.getProductId())) {
                revenue += popularProductsMap.get(item.getProductId());
                popularProductsMap.put(item.getProductId(), revenue);
            } else {
                popularProductsMap.put(item.getProductId(), revenue);
            }
        }

        List<Map.Entry<Integer, Integer>> sortedProductlist
                = new ArrayList<Map.Entry<Integer, Integer>>(popularProductsMap.entrySet());

        Collections.sort(sortedProductlist, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> revenueObj1, Map.Entry<Integer, Integer> revenueObj2) {
                return revenueObj2.getValue() - revenueObj1.getValue();
            }
        });

        System.out.println("________________________________________");
        System.out.println("***Top 3 popular products:***");

        for (int i = 0; i < sortedProductlist.size() && i < 3; i++) {
            System.out.println("Product ID:" + sortedProductlist.get(i).getKey() + ", Revenue:" + sortedProductlist.get(i).getValue());
        }
        System.out.println("________________________________________");

    }

    public void getThreeBestCustomers() {
        Map<Integer, Customer> customerMap = DataStore.getInstance().getCustomersEntityMap();
        Map<Integer, Integer> revenueMap = new HashMap<>();

        for (Customer customer : customerMap.values()) {
            List<Order> orders = customer.getOrdersList();
            int revenue = 0;
            for (Order order : orders) {
                int minPrice = DataStore.getInstance().getProductEntityMap().get(order.getItem().getProductId()).getMinPrice();
                int salesPrice = order.getItem().getSalesPrice();
                int quantity = order.getItem().getQuantity();
                int actualSalesPrice = salesPrice - minPrice;
                revenue += actualSalesPrice * quantity;
            }
            revenueMap.put(customer.getCustomerId(), revenue);
        }

        List<Map.Entry<Integer, Integer>> list
                = new ArrayList<Map.Entry<Integer, Integer>>(revenueMap.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> revenueObj1, Map.Entry<Integer, Integer> revenueObj2) {
                return revenueObj2.getValue() - revenueObj1.getValue();
            }
        });

        System.out.println("Our 3 best customers:");
        System.out.println("______________");

        for (int i = 0; i < list.size() && i < 3; i++) {
            System.out.println("Customer ID:" + list.get(i).getKey() + " ,Revenue:" + list.get(i).getValue());
        }
        System.out.println("______________");

    }
    
    
    public void getTopThreeBestSalesPeople() {

        Map<Integer, SalesPerson> salesMap = DataStore.getInstance().getSalesPersonEntityMap();
        Map<Integer, Integer> revenueMap = new HashMap<>();

        for (SalesPerson salesPerson : salesMap.values()) {
            List<Order> orders = salesPerson.getOrdersList();
            int revenue = 0;
            for (Order order : orders) {
                int minPrice = DataStore.getInstance().getProductEntityMap().get(order.getItem().getProductId()).getMinPrice();
                int salesPrice = order.getItem().getSalesPrice();
                int quantity = order.getItem().getQuantity();
                int actualSalesPrice = salesPrice - minPrice;
                revenue += (actualSalesPrice) * quantity;
            }
            revenueMap.put(salesPerson.getSupplierId(), revenue);
        }

        List<Map.Entry<Integer, Integer>> arraylist
                = new ArrayList<Map.Entry<Integer, Integer>>(revenueMap.entrySet());

        Collections.sort(arraylist, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> revenueObj1, Map.Entry<Integer, Integer> revenueObj2) {
                return revenueObj2.getValue() - revenueObj1.getValue();
            }
        });

        System.out.println("Our top 3 best sales people");
        System.out.println("______________");

        for (int i = 0; i < arraylist.size() && i < 3; i++) {
            System.out.println("Sales ID:" + arraylist.get(i).getKey() + " ,Revenue:" + arraylist.get(i).getValue());
        }
        System.out.println("______________");

    }
    
    public void getTotalRevenueOfYear() {

        Map<Integer, Order> orderMap = DataStore.getInstance().getOrderEntityMap();
        Map<Integer, Product> productMap = DataStore.getInstance().getProductEntityMap();
        int totalRevenue = 0;
        for (Order order : orderMap.values()) {
            int revenue = (order.getItem().getSalesPrice() - productMap.get(order.getItem().getProductId()).getMinPrice()) * order.getItem().getQuantity();
            totalRevenue += revenue;
        }

        System.out.println("Our total revenue for the year: " + totalRevenue);
    }
    
    
}
