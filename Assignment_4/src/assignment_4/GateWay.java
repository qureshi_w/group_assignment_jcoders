/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4;

import assignment_4.analyticsHelper.*;
import assignment_4.entities.*;
import java.io.IOException;
import java.util.Map;

/**
 *
 * @author harshalneelkamal
 */
public class GateWay {

    DataReader orderReaderObj;
    DataReader productReaderObj;
    Analysis helperObj;

    public GateWay() throws IOException {
        DataGenerator generator = DataGenerator.getInstance();
        this.orderReaderObj = new DataReader(generator.getOrderFilePath());
        this.productReaderObj = new DataReader(generator.getProductCataloguePath());
        helperObj = new Analysis();
    }

    public static void main(String args[]) throws IOException {

        GateWay gateway = new GateWay();
        gateway.pupulateData();
        gateway.runAnalysis();

    }

    private void pupulateData() throws IOException {

        String[] orderRowArray;
        while ((orderRowArray = orderReaderObj.getNextRow()) != null) {
            Item item = this.mapItem(orderRowArray);
            Order order = this.mapOrder(orderRowArray, item);
            this.mapCustomer(orderRowArray, order);
            this.mapSales(orderRowArray, order);
        }

        String[] productRowArray;
        while ((productRowArray = productReaderObj.getNextRow()) != null) {
            this.mapProduct(productRowArray);
        }
    }

    private Item mapItem(String[] row) {

        int itemId = Integer.parseInt(row[1]);
        int productId = Integer.parseInt(row[2]);
        int quantity = Integer.parseInt(row[3]);
        int salesPrice = Integer.parseInt(row[6]);

        Item item = new Item(itemId, productId, quantity, salesPrice);
        DataStore.getInstance().getItemEntityMap().put(itemId, item);
        return item;
    }

    private Order mapOrder(String[] row, Item item) {

        int orderId = Integer.parseInt(row[0]);
        int salesId = Integer.parseInt(row[4]);
        int customerId = Integer.parseInt(row[5]);

        Order order = new Order(orderId, salesId, customerId, item);
        DataStore.getInstance().getOrderEntityMap().put(orderId, order);
        return order;
    }

    private void mapCustomer(String[] row, Order order) {

        int customerId = Integer.parseInt(row[5]);

        Map<Integer, Customer> customerMap = DataStore.getInstance().getCustomersEntityMap();

        if (customerMap.containsKey(customerId)) {
            customerMap.get(customerId).getOrdersList().add(order);
        } else {
            Customer customer = new Customer(customerId);
            customer.getOrdersList().add(order);
            customerMap.put(customerId, customer);
        }
    }

    private void mapSales(String row[], Order order) {

        int supplierId = Integer.parseInt(row[4]);

        Map<Integer, SalesPerson> salesMap = DataStore.getInstance().getSalesPersonEntityMap();

        if (salesMap.containsKey(supplierId)) {
            salesMap.get(supplierId).getOrdersList().add(order);
        } else {
            SalesPerson salesPersonObj = new SalesPerson(supplierId);
            salesPersonObj.getOrdersList().add(order);
            salesMap.put(supplierId, salesPersonObj);
        }
    }

    private void mapProduct(String[] row) {

        int productId = Integer.parseInt(row[0]);
        int minPrice = Integer.parseInt(row[1]);
        int maxPrice = Integer.parseInt(row[2]);
        int targetPrice = Integer.parseInt(row[3]);
        Product productObj = new Product(productId, minPrice, maxPrice, targetPrice);
        DataStore.getInstance().getProductEntityMap().put(productId, productObj);

    }

    private void runAnalysis() {
        helperObj.gettopthreeMostPopularProducts();
        helperObj.getTopThreeBestSalesPeople();
        helperObj.getTotalRevenueOfYear();
        helperObj.getThreeBestCustomers();
    }

}
