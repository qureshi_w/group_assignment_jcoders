#Implemented Java/Swing application for the following assignments:
######1. Lab 5 - implemented the concept of Abstract class, association and used regex for validation. 
######2. Lab 8 - implemented methods to generate objects from read files and ran analysis in memory 
######3. Assignment 4 - implemented object model by mapping it to java classes and populated the model. Also, read data from files and aggregated the data and output to a report in the form of a dashboard.

####Requirement and dependency: IDE(NetBeans)


####How to Run the program:
1.	Open the project
2.	Right click on the project and run or press shift + F6 on mainfram.java.


